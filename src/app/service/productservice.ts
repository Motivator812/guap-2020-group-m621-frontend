import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProductService {

  constructor(private http: HttpClient) { }

  getMushroomsByName(name: string) {
    return this.http.get<any>('http://localhost:10080/products/search?name=' + name);
  }

}