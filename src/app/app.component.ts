import { Component } from '@angular/core';
import { ProductService } from './service/productservice';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ProductService]
})
export class AppComponent {

  title = 'mushroom-frontend';
  mushroomName = '';

  mushrooms: Mushroom[];

  constructor(private productService: ProductService) {
  }

  findMushrooms() {
    console.log(this.mushroomName);
    this.productService.getMushroomsByName(this.mushroomName).subscribe(data => this.mushrooms = data);
  }


}
